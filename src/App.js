import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { Beranda, Bola, Detail, Dunia, Ekonomi, GayaHidup, Hiburan, Hukum, Olahraga, Otomotif, Politik, Splash, Teknologi, Terbaru } from './Screen';

const Stack = createNativeStackNavigator();

const App = () => {
  return (
     <NavigationContainer>
      <Stack.Navigator initialRouteName='Splash'>
        <Stack.Screen name="Splash" component={Splash} options={{ headerShown: false, }}/>
        <Stack.Screen name="Beranda" component={Beranda} options={{ headerShown: false, }}/>
        <Stack.Screen name="Bola" component={Bola} options={{ headerShown: false, }}/>
        <Stack.Screen name="Terbaru" component={Terbaru} options={{ headerShown: false, }}/>
        <Stack.Screen name="Politik" component={Politik} options={{ headerShown: false, }}/>
        <Stack.Screen name="Hukum" component={Hukum} options={{ headerShown: false, }}/>
        <Stack.Screen name="Ekonomi" component={Ekonomi} options={{ headerShown: false, }}/>
        <Stack.Screen name="Olahraga" component={Olahraga} options={{ headerShown: false, }}/>
        <Stack.Screen name="GayaHidup" component={GayaHidup} options={{ headerShown: false, }}/>
        <Stack.Screen name="Hiburan" component={Hiburan} options={{ headerShown: false, }}/>
        <Stack.Screen name="Dunia" component={Dunia} options={{ headerShown: false, }}/>
        <Stack.Screen name="Teknologi" component={Teknologi} options={{ headerShown: false, }}/>
        <Stack.Screen name="Otomotif" component={Otomotif} options={{ headerShown: false, }}/>
        <Stack.Screen name="Detail" component={Detail} options={{ headerShown: false, }}/>
      </Stack.Navigator>
    </NavigationContainer>
  )
}

export default App

const styles = StyleSheet.create({})
import Bola from "./Bola";
import Beranda from "./Beranda";
import Terbaru from "./Terbaru";
import Politik from "./Politik";
import Hukum from "./Hukum";
import Ekonomi from "./Ekonomi";
import Olahraga from "./Olahraga";
import GayaHidup from "./GayaHidup";
import Hiburan from "./Hiburan";
import Dunia from "./Dunia";
import Teknologi from "./Teknologi";
import Otomotif from "./Otomotif";
import Splash from "./Splash";
import Detail from "./Detail";


export { 
    Bola, Beranda, Terbaru, Politik, Hukum, Ekonomi, Olahraga, GayaHidup, Hiburan, Dunia, Teknologi, Otomotif, Splash, Detail,
};
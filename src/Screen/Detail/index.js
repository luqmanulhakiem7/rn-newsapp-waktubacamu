import { StyleSheet, Text, View } from 'react-native'
import React, { useEffect, useState } from 'react'
import axios from 'axios';
import { useRoute } from '@react-navigation/native';

const Detail = () => {
    const route = useRoute();
    const title = route.title;

    const [baru, SetBaru] = useState([]);

    useEffect(() => {
        getBaru();
    }, [])

    async function getBaru(){
        try {   
        const res = await axios.get(`${BASE_URL}antara/terbaru/${title}`);
        const result = {
            baru: res.data.data,
        };
        console.log(result)
        if (res) {
            SetBaru(res.data.data.posts)
        }
        } catch (error) {
        console.log(error)
        }
    }

  return (
    <View>
      <Text>Detail</Text>
    </View>
  )
}

export default Detail

const styles = StyleSheet.create({})
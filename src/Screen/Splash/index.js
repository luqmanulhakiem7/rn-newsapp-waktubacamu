import { StyleSheet, Text, View } from 'react-native'
import React, { useEffect } from 'react'

const Splash = ({navigation}) => {

    useEffect(() => {
      setTimeout( () => {
        navigation.replace('Beranda');
      }, 2000);
  }, [navigation]);

  return (
    <View style={styles.position}>
      <Text style={styles.logo}><Text style={{color: '#0096ff',}}>Waktu</Text>Bacamu</Text>
      <Text style={styles.credit}>By Luqmanul Hakiem</Text>
    </View>
  )
}

export default Splash

const styles = StyleSheet.create({
position: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
},
logo: {
  fontWeight: 'bold',
  fontSize: 25,
  color: '#414141',
  borderBottomColor: 'black',
  borderBottomWidth: 2,
  width: 200,
  fontSize: 30,
},
credit: {
    color: 'black',
    fontFamily: 'monospace',
},
})
import { Image, ImageBackground, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React, { useEffect, useState } from 'react'
import { WARNA_1, WARNA_BG, WARNA_TEXT } from '../../assets/warna'
import { BASE_URL } from '../../utils/api';
import axios from 'axios';
import { NavigationHelpersContext } from '@react-navigation/native';
import { ImgBola, ImgDunia, ImgEkonomi, ImgHiburan, ImgHukum, ImgLifestyle, ImgOlahraga, ImgOtomotif, ImgPolitik, ImgTekno } from '../../assets';

const Beranda = ({navigation}) => {

  const [hangat, setHangat] = useState([]);
  const [baru, setTerbaru] = useState([]);

  useEffect(() => {
    GetData();
    GetBaru();
  }, [])

  async function GetData(){
    try {
      const res = await axios.get(`${BASE_URL}antara/terbaru`);
      const result = {
        hangat: res.data.data.posts.splice(0, 6),
      };
      console.log(result)
      if (res) {
        setHangat(res.data.data.posts.splice(0, 6))
      }
    } catch (error) {
      console.log(error)
    }
  }
  
  async function GetBaru(){
    try {
      const res = await axios.get(`${BASE_URL}antara/terbaru`);
      const baru = {
        baru: res.data.data.posts.splice(7, 5),
      };
      console.log(baru)
      if (res) {
        setTerbaru(res.data.data.posts.splice(5, 10))
      }
    } catch (error) {
      console.log(error)
    }
  }
  return (
    <View>
      <View style={styles.header}>
          <Text style={styles.headertxt}><Text style={{color: '#0096ff'}}>Waktu</Text>Bacamu</Text>
      </View>
      <View style={styles.body}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.container}>
            <Text style={styles.title}>Berita Terhangat</Text>
            <ScrollView style={styles.Ctop} horizontal={true} showsHorizontalScrollIndicator={false}>
              {hangat.map((hangat, key) => {
                return (
                <TouchableOpacity style={styles.cardTop} key={key} onPress={() => navigation.navigate('Bola')}>
                  <Image source={{uri:`${hangat.thumbnail}`}} style={styles.cardTopThumb}/>
                  <Text style={styles.cardTopTitle}>{hangat.title}</Text>
                </TouchableOpacity>
                )
              })}
              <View style={{paddingRight: 20,}}>
              </View>
            </ScrollView>
          </View>

          <View style={styles.container}>
            <Text style={styles.title}>Jelajah</Text>
            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={styles.Ctop}>
              <TouchableOpacity onPress={() => navigation.navigate("Politik")}>
                <ImageBackground source={ImgPolitik} style={styles.Kategori}>
                  <Text style={styles.menu}>Politik</Text>
                </ImageBackground>   
              </TouchableOpacity>
              <TouchableOpacity onPress={() => navigation.navigate("Hukum")}>
                <ImageBackground source={ImgHukum} style={styles.Kategori}>
                  <Text style={styles.menu}>Hukum</Text>
                </ImageBackground>   
              </TouchableOpacity>
              <TouchableOpacity onPress={() => navigation.navigate("Ekonomi")}>
                <ImageBackground source={ImgEkonomi} style={styles.Kategori}>
                  <Text style={styles.menu}>Ekonomi</Text>
                </ImageBackground>   
              </TouchableOpacity>
              <TouchableOpacity onPress={() => navigation.navigate("Bola")}>
                <ImageBackground source={ImgBola} style={styles.Kategori}>
                  <Text style={styles.menu}>Bola</Text>
                </ImageBackground>     
              </TouchableOpacity>
              <TouchableOpacity onPress={() => navigation.navigate("Olahraga")}>
                <ImageBackground source={ImgOlahraga} style={styles.Kategori}>
                  <Text style={styles.menu}>Olahraga</Text>
                </ImageBackground>     
              </TouchableOpacity>
              <TouchableOpacity onPress={() => navigation.navigate("GayaHidup")}>
                <ImageBackground source={ImgLifestyle} style={styles.Kategori}>
                  <Text style={styles.menu}>Gaya Hidup</Text>
                </ImageBackground>     
              </TouchableOpacity>
              <TouchableOpacity onPress={() => navigation.navigate("Hiburan")}>
                <ImageBackground source={ImgHiburan} style={styles.Kategori}>
                  <Text style={styles.menu}>Hiburan</Text>
                </ImageBackground>     
              </TouchableOpacity>
              <TouchableOpacity onPress={() => navigation.navigate("Dunia")}>
                <ImageBackground source={ImgDunia} style={styles.Kategori}>
                  <Text style={styles.menu}>Dunia</Text>
                </ImageBackground>     
              </TouchableOpacity>
              <TouchableOpacity onPress={() => navigation.navigate("Teknologi")}>
                <ImageBackground source={ImgTekno} style={styles.Kategori}>
                  <Text style={styles.menu}>Teknologi</Text>
                </ImageBackground>     
              </TouchableOpacity>
              <TouchableOpacity onPress={() => navigation.navigate("Otomotif")}>
                <ImageBackground source={ImgOtomotif} style={styles.Kategori}>
                  <Text style={styles.menu}>Otomotif</Text>
                </ImageBackground>     
              </TouchableOpacity>
              
              <View style={{paddingRight: 20,}}>
              </View> 
            </ScrollView>
          </View>

          <View style={styles.container}>
            <View style={styles.title2}>
              <Text style={styles.titletxt}>Berita Terbaru</Text>
              <TouchableOpacity onPress={() => navigation.navigate('Terbaru')}>
                <Text style={styles.titleImp}>Semua</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.container2}>
              {baru.map((baru, kunci) => {
                return (
                  <TouchableOpacity style={styles.rekom} key={kunci} onPress={() => navigation.navigate('Detail', {title: baru.title})}>
                    <View style={styles.CbotCon}>
                      <Text style={styles.CbotTxt}>{baru.title}</Text>
                    </View>
                    <View>
                      <Image source={{uri: `${baru.thumbnail}`}} style={styles.Cbot}/>
                    </View>
                  </TouchableOpacity>
                )
              })}
            </View>
            <View style={styles.container2}>
            </View>
          </View>
        </ScrollView>
        <View style={{marginBottom: 20,}}>
        </View>
      </View>
    </View>
  )
}

export default Beranda

const styles = StyleSheet.create({
body: {
  color: WARNA_1,
  width: '100%',
  height: '100%',
},
header: {
  width: '100%',
  height: 50,
  justifyContent: 'center',
  alignItems: 'center',
  backgroundColor: '#fff',
  borderBottomWidth: 5,
},
headertxt: {
  fontWeight: 'bold',
  fontSize: 25,
  color: '#414141',
},

title: {
  padding: 20,
  fontSize: 15,
  fontWeight: '500',
  color: WARNA_TEXT,
},
title2: {
  padding: 20,
  flexDirection: 'row',
  justifyContent: 'space-between',
},
titletxt: {
  fontSize: 15,
  fontWeight: '500',
  color: WARNA_TEXT,
},
titleImp: {
  fontSize: 15,
  fontWeight: '500',
  color: '#0096ff',
},
container: {
  marginBottom: 20,
},
container2: {
  padding: 20,
},

Ctop: {
  paddingLeft: 20,
  paddingRight: 20,

},
cardTop: {
  backgroundColor: WARNA_1,
  width: 350,
  height: 250,
  borderRadius: 5,
  marginRight: 10,
  borderWidth: 2,
  borderColor: WARNA_BG,
  overflow: 'hidden',
},
cardTopThumb: {
  height: 180,
  width: 400,
  borderTopLeftRadius: 5,
  borderTopRightRadius: 5,
  marginBottom: 4,
},
cardTopTitle: {
  padding: 10,
  fontSize: 14,
  color: WARNA_TEXT,
  fontWeight: '500',
},
Kategori: {
  width: 70,
  height: 70,
  backgroundColor: WARNA_BG,
  borderRadius: 40,
  marginRight: 10,
  justifyContent: 'center',
  alignItems: 'center',
  overflow: 'hidden',
  borderWidth: 2,
},
menu: {
  color: '#fff', 
  fontWeight: 'bold', 
  textShadowColor: 'rgba(0, 0, 0, 0.75)',
  textShadowOffset: {width: -1, height: 1},
  textShadowRadius: 10, 
},
rekom: {
  width: 350,
  height: 100,
  backgroundColor: WARNA_1,
  borderRadius: 5,
  marginBottom: 10,
  flexDirection: 'row',
  overflow: 'hidden',
  borderWidth: 2,
  borderColor: WARNA_BG,
},
Cbot: {
  width: 120,
  height:100,
  borderTopRightRadius: 5,
  borderBottomRightRadius: 5,
},
CbotCon: {
  width: 240,
  padding: 20,
},
CbotTxt: {
  color: WARNA_TEXT,
  fontWeight: '500',
},

})
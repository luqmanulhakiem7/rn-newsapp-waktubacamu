import { Image, ImageBackground, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React, { useEffect, useState } from 'react'
import { ImgBola, ImgDunia, ImgEkonomi, ImgHiburan, ImgHukum, ImgLifestyle, ImgOlahraga, ImgOtomotif, ImgPolitik, ImgTekno } from '../../assets';
import { WARNA_1, WARNA_BG, WARNA_TEXT } from '../../assets/warna';
import axios from 'axios';
import { BASE_URL } from '../../utils/api';

const Ekonomi = ({navigation}) => {
  const [ekonomi, setEkonomi] = useState([]);

    useEffect(() => {
        getEkonomi();
    }, [])

    async function getEkonomi(){
        try {   
        const res = await axios.get(`${BASE_URL}antara/ekonomi`);
        const result = {
            ekonomi: res.data.data.posts,
        };
        console.log(result)
        if (res) {
            setEkonomi(res.data.data.posts)
        }
        } catch (error) {
        console.log(error)
        }
    }
  return (
     <View>
      <View style={styles.header}>
        <TouchableOpacity onPress={() => navigation.navigate("Beranda")}>
          <Text style={styles.headertxt}><Text style={{color: '#0096ff'}}>Waktu</Text>Bacamu</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.body}>
         <View style={styles.container}>
            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={styles.Ctop}>
                 <View style={{paddingLeft: 20,}}>
              </View> 
               <TouchableOpacity onPress={() => navigation.navigate("Politik")}>
                <ImageBackground source={ImgPolitik} style={styles.Kategori}>
                  <Text style={styles.menu}>Politik</Text>
                </ImageBackground>   
              </TouchableOpacity>
              <TouchableOpacity onPress={() => navigation.navigate("Hukum")}>
                <ImageBackground source={ImgHukum} style={styles.Kategori}>
                  <Text style={styles.menu}>Hukum</Text>
                </ImageBackground>   
              </TouchableOpacity>
              <TouchableOpacity onPress={() => navigation.navigate("Ekonomi")}>
                <ImageBackground source={ImgEkonomi} style={styles.Kategori}>
                  <Text style={styles.menu}>Ekonomi</Text>
                </ImageBackground>   
              </TouchableOpacity>
              <TouchableOpacity onPress={() => navigation.navigate("Bola")}>
                <ImageBackground source={ImgBola} style={styles.Kategori}>
                  <Text style={styles.menu}>Bola</Text>
                </ImageBackground>     
              </TouchableOpacity>
              <TouchableOpacity onPress={() => navigation.navigate("Olahraga")}>
                <ImageBackground source={ImgOlahraga} style={styles.Kategori}>
                  <Text style={styles.menu}>Olahraga</Text>
                </ImageBackground>     
              </TouchableOpacity>
              <TouchableOpacity onPress={() => navigation.navigate("GayaHidup")}>
                <ImageBackground source={ImgLifestyle} style={styles.Kategori}>
                  <Text style={styles.menu}>Gaya Hidup</Text>
                </ImageBackground>     
              </TouchableOpacity>
              <TouchableOpacity onPress={() => navigation.navigate("Hiburan")}>
                <ImageBackground source={ImgHiburan} style={styles.Kategori}>
                  <Text style={styles.menu}>Hiburan</Text>
                </ImageBackground>     
              </TouchableOpacity>
              <TouchableOpacity onPress={() => navigation.navigate("Dunia")}>
                <ImageBackground source={ImgDunia} style={styles.Kategori}>
                  <Text style={styles.menu}>Dunia</Text>
                </ImageBackground>     
              </TouchableOpacity>
              <TouchableOpacity onPress={() => navigation.navigate("Teknologi")}>
                <ImageBackground source={ImgTekno} style={styles.Kategori}>
                  <Text style={styles.menu}>Teknologi</Text>
                </ImageBackground>     
              </TouchableOpacity>
              <TouchableOpacity onPress={() => navigation.navigate("Otomotif")}>
                <ImageBackground source={ImgOtomotif} style={styles.Kategori}>
                  <Text style={styles.menu}>Otomotif</Text>
                </ImageBackground>     
              </TouchableOpacity>
              <View style={{paddingRight: 20,}}>
              </View> 
            </ScrollView>
          </View>
          <View style={styles.container}>
            <View style={styles.TopCard}>
                <ScrollView style={styles.TopC} showsVerticalScrollIndicator={false}>
                    <Text style={styles.title}>Berita Ekonomi</Text>
                    {ekonomi.map((ekonomi, key) => {
                        return (
                        <TouchableOpacity style={styles.cardTop} key={key} onPress={() => navigation.navigate('Bola')}>
                        <Image source={{uri:`${ekonomi.thumbnail}`}} style={styles.cardTopThumb}/>
                        <Text style={styles.cardTopTitle}>{ekonomi.title}</Text>
                        </TouchableOpacity>
                        )
                    })}
                    <View style={{marginTop: 150,}}>
                    </View> 
                </ScrollView>
            </View>
          </View>
      </View>
    </View>
  )
}

export default Ekonomi

const styles = StyleSheet.create({
body: {
  color: WARNA_1,
  width: '100%',
  height: '100%',
},
header: {
  width: '100%',
  height: 50,
  justifyContent: 'center',
  alignItems: 'center',
  backgroundColor: '#fff',
  borderBottomWidth: 5,
},
headertxt: {
  fontWeight: 'bold',
  fontSize: 25,
  color: '#414141',
},
container: {
  marginBottom: 5,
},
title: {
  padding: 20,
  fontSize: 15,
  fontWeight: '500',
  color: WARNA_TEXT,
},
Kategori: {
  width: 70,
  height: 70,
  backgroundColor: WARNA_BG,
  borderRadius: 40,
  marginRight: 10,
  justifyContent: 'center',
  alignItems: 'center',
  overflow: 'hidden',
  borderWidth: 2,
},
menu: {
  color: '#fff', 
  fontWeight: 'bold', 
  textShadowColor: 'rgba(0, 0, 0, 0.75)',
  textShadowOffset: {width: -1, height: 1},
  textShadowRadius: 10, 
},

TopCard: {
    justifyContent: 'center',
    margin: 20,
},
TopC: {
    marginBottom: 20,
},
Ctop: {
    marginTop: 10,
    marginBottom: 0,
},
cardTop: {
  backgroundColor: WARNA_1,
  width: 350,
  height: 250,
  borderRadius: 5,
  marginBottom: 20,
  borderWidth: 2,
  borderColor: WARNA_BG,
  overflow: 'hidden',
},
cardTopThumb: {
  height: 180,
  width: 400,
  borderTopLeftRadius: 5,
  borderTopRightRadius: 5,
  marginBottom: 4,
},
cardTopTitle: {
  padding: 10,
  fontSize: 14,
  color: WARNA_TEXT,
  fontWeight: '500',
},
})
import ImgBola from './bola.jpg'
import ImgPolitik from './politik.jpg'
import ImgDunia from './dunia.jpg'
import ImgHukum from './hukum.jpg'
import ImgLifestyle from './lifestyle.jpg'
import ImgOlahraga from './olahraga.jpg'
import ImgOtomotif from './otomotif.jpg'
import ImgTekno from './tekno.jpg'
import ImgEkonomi from './ekonomi.jpg'
import ImgHiburan from './hiburan.jpg'

export {
    ImgBola, ImgPolitik, ImgDunia, ImgHukum, ImgLifestyle, ImgOlahraga, ImgOtomotif, ImgTekno, ImgEkonomi, ImgHiburan
}